
FROM node:10 as builder

WORKDIR /work
RUN echo '{ "allow_root": true }' > /root/.bowerrc

COPY package.json ./

RUN npm install

COPY . .
RUN npm install
RUN npm run build
RUN npm run test


FROM nginx:1.15-alpine
COPY --from=builder /work/public/* /usr/share/nginx/html/

